FROM ubuntu:16.04
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y nano openjdk-8-jdk curl
RUN curl -L http://bob.nem.ninja/nis-0.6.95.tgz > nis-0.6.95.tgz
RUN tar -xf nis-0.6.95.tgz
VOLUME ["/root/nem"]
VOLUME ["/package/nis"]
EXPOSE 7890
WORKDIR /package
CMD ./nix.runNis.sh
#CMD yes
# docker run --rm --name nem -v ~/nem:/root/nem -v ~/package/nis:/package/nis -p 7890:7890 nem
